# Postnames Service Project

Simple REST API in spring boot that accepts a list of Names and Postcodes in
the HTTP request body, and persist the data in a database.
The API have an endpoint that receives the postcode range and 
returns in the response body the list of names belonging to that postcode range, 
sorted alphabetically as well as the total number of characters of all names 
combined.

## Design and Implementation
### Databases
  - Docker/Prod build is using MySQL on port 3306.
  - Test config is using HSQL in-memory database.
  - API runs on port 8080 by default
### Rest API
   Rest API has two endpoints 

| Method | URL  | Param | Body |
| ----- | ------------------------------|------------|-------------|
| POST | /save-postcode-names | N/A |list of postcode names
| GET  | /list-postcode-names | postnames=list of postcodes| N/A
    
### Docker Test
 Docker must be installed locally for this to work.
 To run inside docker container follow these commands. 
    
`./gradlew jibDockerBuild
 cd integration-docker-test
docker-compose up -d`

 First it will create a local docker image for  API project.
 Then it will start mysql server container and postnames API container.
 
 mysql will be running on port 3306 and postnames  API on port 8080.
 The services will be accessible from localhost

### Testing with Postman

The solution can be tested with Postman.
The distribution includes `Postnames.postman_collection.json` file 
which contains exported postman collection. This file can be imported 
into Postman. It contains simple examples of API calls.

![](post-test.jpg)
![](get-test.jpg)

