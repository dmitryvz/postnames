package com.postname.chalenge.util;

import java.util.List;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;

@UtilityClass
public class PostcodeNameUtils {

  public Long charCount(List<String> stringList) {
    return stringList.stream()
        .collect(Collectors.summarizingLong(String::length))
        .getSum();
  }
}
