package com.postname.chalenge.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Data
@Table(name = "postcode_name")
public class PostcodeName {

  @Id
  @GeneratedValue
  private Long id;
  private Integer postcode;
  private String name;
}
