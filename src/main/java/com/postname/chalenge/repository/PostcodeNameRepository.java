package com.postname.chalenge.repository;

import com.postname.chalenge.model.PostcodeName;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostcodeNameRepository extends JpaRepository<PostcodeName, Long> {

  List<PostcodeName> findPostcodeNamesByPostcodeInOrderByName(List<Integer> postcode);
}
