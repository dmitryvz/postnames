package com.postname.chalenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude = {
    org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class
})
public class PostnameApplication {

  public static void main(String[] args) {
    SpringApplication.run(PostnameApplication.class, args);
  }

}
