package com.postname.chalenge.rest;

import com.postname.chalenge.model.PostcodeName;
import com.postname.chalenge.service.PostcodeNameService;
import com.postname.chalenge.util.PostcodeNameUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostcodeNameController {

  private final PostcodeNameService postcodeNameService;

  @Autowired
  public PostcodeNameController(PostcodeNameService postcodeNameService) {
    this.postcodeNameService = postcodeNameService;
  }

  @PostMapping("/save-postcode-names")
  public List<PostcodeName> savePostcodeNames(@RequestBody List<PostcodeName> postcodeNames) {
    return postcodeNameService.savePostcodeNames(postcodeNames);
  }

  @GetMapping("/list-postcode-names")
  public PostcodeNameResponse listPostcodeNames(@RequestParam("postcodes") List<Integer> postcodes) {
    List<String> names = postcodeNameService.getPostcodeNames(postcodes);
    return PostcodeNameResponse
        .builder()
        .names(names)
        .charCount(PostcodeNameUtils.charCount(names))
        .build();
  }
}
