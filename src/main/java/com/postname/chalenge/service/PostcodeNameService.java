package com.postname.chalenge.service;

import com.postname.chalenge.model.PostcodeName;
import com.postname.chalenge.repository.PostcodeNameRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@AllArgsConstructor
public class PostcodeNameService {

  private final PostcodeNameRepository postcodeNameRepository;

  @Transactional
  public List<PostcodeName> savePostcodeNames(List<PostcodeName> postcodeNames) {
    return postcodeNameRepository.saveAll(postcodeNames);
  }

  public List<String> getPostcodeNames(List<Integer> postcodes) {
    return postcodeNameRepository.findPostcodeNamesByPostcodeInOrderByName(postcodes)
        .stream()
        .map(PostcodeName::getName)
        .collect(Collectors.toList());
  }

}
