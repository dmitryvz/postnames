package com.postname.chalenge.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class PostcodeNameUtilsTest {

  @Test
  @DisplayName("The number of characters in the provided list of strings should be equal to 31 ")
  void charCount(){
    var list= Arrays.asList("Osborne Park", "Tuart Hill", "Claremont");
    assertThat(PostcodeNameUtils.charCount(list)).isEqualTo(31);
  }
}
