package com.postname.chalenge.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import com.postname.chalenge.model.PostcodeName;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
//@TestPropertySource(
//    locations = "classpath:application-test.yml")
public class PostcodeNameServiceTest {

  private static Faker faker = Faker.instance();
  @Autowired
  PostcodeNameService postcodeNameService;

  @Test
  @DisplayName("Should create list of 10 names and postcodes ")
  void create() {
    List<PostcodeName> list =
        IntStream.range(0, 9)
            .mapToObj(i ->
                PostcodeName.builder()
                    .name(faker.address().cityName())
                    .postcode(faker.random().nextInt(4))
                    .build())
            .collect(Collectors.toList());
    postcodeNameService.savePostcodeNames(list);
    List<String> names = postcodeNameService.getPostcodeNames(
        list.stream()
            .map(PostcodeName::getPostcode)
            .collect(Collectors.toList()));
    assertThat(names)
        .containsExactlyInAnyOrderElementsOf(list.stream()
            .map(PostcodeName::getName)
            .collect(Collectors.toList()));
  }

}
