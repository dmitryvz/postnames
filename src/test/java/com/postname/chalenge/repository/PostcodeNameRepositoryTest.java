package com.postname.chalenge.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import com.postname.chalenge.model.PostcodeName;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class PostcodeNameRepositoryTest {

  private static Faker faker = Faker.instance();

  @Autowired
  private PostcodeNameRepository postcodeNameRepository;

  @Test
  @DisplayName("Should create list of 10 names and postcodes and thn retrieve it from database ")
  void findPostcodeNamesByPostcodeInOrderByNameTest() {
    List<PostcodeName> list =
        IntStream.range(0, 9)
            .mapToObj(i ->
                PostcodeName.builder()
                    .name(faker.address().cityName())
                    .postcode(faker.random().nextInt(4))
                    .build())
            .collect(Collectors.toList());
    postcodeNameRepository.saveAll(list);
    List<PostcodeName> postcodeNames = postcodeNameRepository.findPostcodeNamesByPostcodeInOrderByName(
        list.stream()
            .map(PostcodeName::getPostcode)
            .collect(Collectors.toList()));
    assertThat(postcodeNames)
        .extracting(PostcodeName::getPostcode)
        .containsExactlyInAnyOrderElementsOf(
            list.stream().map(PostcodeName::getPostcode).collect(Collectors.toList()));
    assertThat(postcodeNames)
        .extracting(PostcodeName::getName)
        .containsExactlyInAnyOrderElementsOf(
            list.stream().map(PostcodeName::getName).collect(Collectors.toList()));
  }
}
