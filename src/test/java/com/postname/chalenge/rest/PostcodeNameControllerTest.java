package com.postname.chalenge.rest;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.postname.chalenge.model.PostcodeName;
import com.postname.chalenge.service.PostcodeNameService;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(value = PostcodeNameController.class, excludeAutoConfiguration = {SecurityAutoConfiguration.class})
public class PostcodeNameControllerTest {


  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private PostcodeNameService service;

  @Test
  @DisplayName("Should retrieve list of 3 postcode names ")
  public void shouldReturnPostcodeNamesFromService() throws Exception {
    when(service.getPostcodeNames(anyList())).thenReturn(List.of("Osborne Park", "Greenwood", "Perth"));
    this.mockMvc.perform(get("/list-postcode-names?postcodes=6060,6024,6000")).andDo(print()).andExpect(status().isOk())
        .andExpect(content().json("{\"names\":[\"Osborne Park\",\"Greenwood\",\"Perth\"],\"charCount\":26}"));
  }

  @Test
  @DisplayName("Should save list of 3 postcode names ")
  public void shouldSavePostcodeNames() throws Exception {
    List<PostcodeName> postcodeNames = List.of(
        PostcodeName.builder().postcode(6060).name("Osborne Park").build(),
        PostcodeName.builder().postcode(6024).name("Greenwood").build(),
        PostcodeName.builder().postcode(6000).name("Perth").build());

    when(service.savePostcodeNames(anyList())).thenReturn(postcodeNames);
    this.mockMvc.perform(post("/save-postcode-names", postcodeNames)
            .content(asJsonString(postcodeNames))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().json(asJsonString(postcodeNames)));
  }

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}